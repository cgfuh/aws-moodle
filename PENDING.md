# Pending activities or improvements

## Some pending tasks or different ways to implement it

- Add crons jobs and activate properly the email system (e.g. sendmail)
- Fine tunning of the database and moodle itself.
- Use valid certificates. Maybe [Let's Encrypt](https://letsencrypt.org/)
- Add some CI/CD to deploy at least in `dev` environment our demo instance.
  Something like [CircleCI](https://circleci.com/), [Bitbucket
  pipelines](https://support.atlassian.com/bitbucket-cloud/docs/build-test-and-deploy-with-pipelines/)
  or [Jenkins](https://www.jenkins.io/) could help.
- Add tests to run at least some Moodle after-installation checks.
- Use docker build images to push to docker registry (or _ECR_) and deploy
  using that packed artifact. Always try go with `inmutable deployments`.
- Make this whole installation kind of `cloud-agnostic` installation. Or at
  least most general and not too `AWS focused`. Forget for example about the
  `user-data` bootstrapping script.
- Move `terraform state` to an `external backend`, such as `S3` or `Terraform cloud`.
