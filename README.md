# Moodle in Amazon AWS

On this guide:

[TOC]

---

## About

This is a guide to install an instance of [_Moodle learning
platform_](https://moodle.com/) into Amazon AWS cloud service.

Two procedures will be described. A [**manual
procedure**](#markdown-header-manual-procedure), where you can go step by step
creating and configuring the needed resources in AWS and satisfying the
requirements by Moodle in order to set up and publish the service in the cloud.

The second one, is the [**automated
procedure**](#markdown-header-automation-procedure), where you'll be using some
automation tools to provisionate the resources and finally install and setup
the moodle instance.

### Scenario

![Alt text](./assets/MoodleAWS.png "Moodle instance at AWS")

These are the tools and software versions used for this demo running instance
in the cloud:

- Apache version `2.4.46`
- PHP version `7.4.15` with some extensions
- PostgreSQL database version `11.5.5`
- Amazon Linux 2 AMI `ami-0ca43e15336e41670` (Sao Paulo region)
- Moodle version `3.10.3`
- Terraform version `v0.14.9`.

## General requirements

- AWS account (with _root_ shell access)
- Moodle software
- Automation tools

Previous knowledge:

- Some hands-on experience in AWS (Console, CLI, etc)
- Linux systems, shell and bash scripting
- Familiarity with some automation tool, i.e Terraform

## Manual procedure

### Requirements

Basically Moodle requires the following software to be installed before hand:

- A web server (e.g. Apache)
- A database (e.g. MySQL, MariaDB or PostgreSQL)
- PHP configured and with some extensions

_Note:_

    See this page in order to know about the software versions required by
    Moodle 3.10.x
    https://docs.moodle.org/dev/Moodle_3.10_release_notes#Server_requirements

#### Cloud AWS resources

We have to prepare our AWS resources in order to get a linux EC2 instance
running in the cloud. Here some steps to follow in order to get that:

1.  Create an EC2 (free-tier t2.micro is fine)

    1.1. Create VPC, subnets and routing staff

    1.2. Create and assign Security Groups, with at least these two required
    inbound rules:

         - SSH access (tcp/22 open for everyone)
         - HTTPS access (tcp/443 open for secure web service)

2.  Create new shell user, group and role for moodle team accesss

    2.1. Create key access and ssh access keys for this user

3.  Give access to the Moodle Group Team to the EC2 instance console.

Once we have our EC2 instance runnning, let's continue with the proper
installation of Moodle software.

### Installing Moodle

The best way to get started is to follow the [Installation
Guide](https://docs.moodle.org/310/en/Installing_Moodle) in the Moodle
Documentation site.

Make sure to satisfy the [minimum hardware
requirements](https://docs.moodle.org/310/en/Installing_Moodle#Hardware).

Here are our steps, which we got and followed from the installation guide.

But before we get started, please `ssh the ec2 instance` by doing from your
terminal:

```sh
    $ ssh -i moodle-team.pem ec2-user@<public_ip>
```

_Note:_

    The "moodle-team.pem" is the key pair generated in our AWS console and
    downloaded to our PC.

    The "<public_ip>" is the current public ip assigned to our EC2 instance.

#### Apache web server installation

```sh
    $ sudo yum install httpd.x86_64
```

Enable and start Apache service:

```sh
    $ sudo systemctl start httpd.service
    $ sudo systemctl enable httpd.service
```

- Enable slasharguments in apache:

```sh
    $ echo "AcceptPathInfo On" > /etc/httpd/conf.d/moodle.conf
```

#### Enable HTTPS

`mod_ssl` will be required to be installed for `HTTPS` access

```sh
    $ sudo yum install -y mod_ssl
```

- First you have to generate a self-signed (for this demo) certificate:

```sh
    $ cd /etc/pki/tls/certs

    $ ./make-dummy-cert moodle-localhost.crt
```

- Edit `/etc/httpd/conf.d/ssl.conf` and comment out the following line:

```
    # SSLCertificateKeyFile /etc/pki/tls/private/localhost.key
```

- Or you can basically set a new ssl config and store it into the
  `/etc/httpd/conf.d` directory.
  For example, update our previous `moodle.conf` located at conf.d directory:

`/etc/httpd/conf.d/moodle.conf`:

```
    AcceptPathInfo On

    <VirtualHost *:443>
        SSLEngine On
        SSLCertificateFile /etc/pki/tls/certs/moodle-localhost.crt
    </VirtualHost>
```

#### PHP and extensions

Some extensions are **required** by Moodle, while others just _recommended_.
We're going to install all the dependencies that Moodle needs:

First, in our especial case with the `Amazon AMI Linux 2` installed, we have to enable the `v7.4` of PHP:

```sh
    $ sudo amazon-linux-extras enable php7.4
```

Here the installation of all the php extensions:

```sh
    $ yum install php \
        php-pear php-xml php-mbstring php-pgsql php-pdo \
        php-opcache php-zip php-zts php-json memcached php-gd \
        php-soap php-xmlrpc php-intl
```

#### PostgreSQL installation

Again, we have to enable the `v11.x` version of Postgres in our `AWS ami Linux 2`:

```sh
    $ sudo amazon-linux-extras enable postgresql11

    $ sudo yum clean metadata

    $ sudo amazon-linux-extras install postgresql11

    $ sudo yum install postgresql-server
```

#### Checking our Apache + PHP + PostgresSQL versions installed

```sh
    $ sudo yum list installed httpd postgresql php postgresql-server

    Loaded plugins: extras_suggestions, langpacks, priorities, update-motd
    Installed Packages

    httpd.x86_64                  2.4.46-1.amzn2      @amzn2-core
    php.x86_64                    7.4.15-1.amzn2      @amzn2extra-php7.4
    postgresql.x86_64             11.5-5.amzn2.0.1    @amzn2extra-postgresql11
    postgresql-server.x86_64      11.5-5.amzn2.0.1    @amzn2extra-postgresql11
```

#### Initialize the postgres database server

```sh
    $ sudo /usr/bin/postgresql-setup --initdb
```

- Start and enable postgresql to start at boot:

```sh
    $ sudo systemctl start postgresql
    $ sudo systemctl enable postgresql
```

#### Adding some checkings to our current installation

We can add somo `phpinfo()` info page to check the http service and the health
of our php installation:

```sh
    $ echo "<?php phpinfo(); ?>" > /var/www/html/phpinfo.php

    $ curl http://localhost/phpinfo.php
```

#### Download the Moodle files

In this case we will be using the latest and stable version compressed found
[here](https://download.moodle.org/)

Latest and stable moodle [`version 3.10.3`](https://download.moodle.org/download.php/direct/stable310/moodle-3.10.3.tgz)

```sh
    $ cd /home/ec2-user

    $ wget https://download.moodle.org/download.php/direct/stable310/moodle-3.10.3.tgz
```

#### Unzip and check moodle files permissions

Untar the downloaded file in the documents site directory of the Apache web
server.

```sh
    $ cd /var/www/html

    $ tar xvfz /home/ec2-user/moodle-3.10.3.tgz
```

Check and verify that files are not writeable by the web server user.

```sh
    $ sudo chown -R root /var/www/html/moodle

    $ sudo chmod -R 0755 /var/www/html/moodle
```

#### Create the moodle database

```sh
    $ sudo -u postgres psql

    postgres=# CREATE USER moodleuser WITH PASSWORD 'moodleuserpassw';

    postgres=# CREATE DATABASE moodle WITH OWNER moodleuser;

    postgres=# \q
```

#### Create the (moodledata) data directory

```sh
    $ sudo mkdir /var/www/moodledata
    $ sudo chmod 0777 /var/www/moodledata
```

_Note:_ This `data directory` must not be accesible via the web.

#### Start the Moodle installation

The following options are available to first-install Moodle:

- Via [`Command line installer`](https://docs.moodle.org/310/en/Administration_via_command_line)

  Being _apache_ the user of the _httpd_ service, use the cli this way to
  execute the `install.php` script:

```sh
    $ sudo chown -R apache:apache /var/www/html/moodle

    $ cd /var/www/html/moodle/admin/cli

    $ sudo -u apache /usr/bin/php install.php --help
```

- Setting up a default `config.php` based on a given `config-dist.php`:

  Copy and edit the config.php according to path, database types and values
  required:

```sh
    $ cd /var/www/html/moodle/

    $ sudo cp config-dist.php config.php

    Edit config.php accordly to your needs.
```

- Going to our instance's public IP and following the installation steps given
  by the Web UI of Moodle:

  Visit `http://<PUBLIC_IP>/moodle` and follow the wizard.

#### Enabling HTTPS in moodle `config.php`

Finally, check and fix the `wwwroot` value for the correct schema (**https**),
in order to bring up the service in a secure way by setting up `config.php`.

We have to edit `config.php` and change `http://` to **`https://`** in
`$CFG->wwwroot`.

## Automation procedure

Will use some `IaC` (Infrastructure as Code) taking in mind one of its
benefits, _reproducible environments_. Furthermore, IaC will help us to have
_disposable environments_, meaning that we can create and destroy our instances
without much effort and time.

### Options to automate our installation

Besides deploying our infrastructure in AWS with the help of
[Terraform](https://terraform.io), we still need to install, configure and
customize this initial deployment on these aws resources.

For this propose, we have some options, like:

a) [Bootstrapping our EC2 machines in AWS (`user_data`)](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html)

b) Building our own docker moodle images.

c) Building and packing with some tools like [Packer](https://www.packer.io/)

d) Using Ansible/Chef/Puppet/Saltstack to automate our configurations

In this chosen scenario to deploy a demo Moodle instance in AWS, we decided
to use:

- **Terraform** and the **option (a)**, bootstrapping the EC2 instance with
  provisioned user-data scripting.

### Requirements

- [`Terraform`](https://terraform.io)
- `Bash scripts`

### Terraform

Some notes before installing and using Terraform for the automated process.

- Due we're just installing Moodle for demo proposes, the `state` of the
  terraform deployment will be saved locally, in your pc.

  **Important**

      We encourage you to go and setup an external backend for keeping the
      terraform state for a production like scenario.

- For customizing our infrastructure deployment, we're using some terraform
  `variables` that need to be adjusted before hand or while executing the
  terraform commands.

  The files and tree file structure of our infrastructure scripts is the
  following:

```
    ├── infra/
    │   ├── main.tf                (provider, templates, ec2 instance staff)
    │   ├── network.tf             (vpc, subnets, security groups staff)
    │   ├── outputs.tf             (some useful output variables)
    │   ├── secrets.tfvars         (secret file, only exist in your workdir,
                                    not in repo, and used for credentials and
                                    secrets for our intallation)
    │   ├── secrets.tfvars.example (example secrets/credentials file)
    │   └── variables.tf           (common variables needed to be adjusted,
                                    some of them have default values already set)
```

That being said, continue with the terraform automation procedure.

#### Download the binary

You will need to download the [terraform
binary](https://www.terraform.io/downloads.html).

Our selected version, at this moment, is `v0.14.9`.

#### Configuring and initializing Terraform

- First step, you should initialize your terraform environment to download the
  plugins required. In this case, the AWS provider plugin.

```sh
    $ cd infra/

    $ terraform init
```

#### Planning and applying the deploy plan

- If you prefer to save the plan, before to apply, you can save its output to a
  file:

```sh
    $ terraform plan -var-file secrets.tfvars -out moodleplan.tf

    $ terraform apply moodleplan.tf
```

- Or just if you want to apply:

```sh
    $ terraform apply -var-file secrets.tfvars
```

_Note:_
We can set our instance name by giving as parameter the name of it, for example:

```sh
    $ terraform apply -var-file secrets.tfvars -var "instance_name=Moodle Demo"
```

- After a successful `apply`, the output result will look like this:

```sh
    Apply complete! Resources: 9 added, 0 changed, 0 destroyed.

    Outputs:

    instance_id = "i-0f54fee9842cd3d40"
    instance_public_ip = "54.233.245.103"
    moodle_https_sg_id = "sg-06e14a209f5d69cf5"
    moodle_ssh_sg_id = "sg-001e2537dd9639583"
    moodle_url = "https://ec2-54-233-245-103.sa-east-1.compute.amazonaws.com/moodle"
```

_Note:_

    The ``moodle_url`` contains the Moodle public URL in this cloud instance.

**Final Note:**

    We are done.

    At this moment, the Moodle demo instance installed should be running and
    accessible for anyone in the AWS cloud.

    Enjoy Moodle ;)

##### Some more useful terraform commands:

- Show your current applied plan:

```sh
    $ terraform show
```

- Validate your files locally:

```sh
    $ terraform validate
```

- Query about the output of the last generated deployment:

```sh
    $ terraform output
```

- Finally when everything was tested, you can destroy your last deploy in AWS:

```sh
    $ terraform destroy -var-file secrets.tfvars
```

This will ask you to confirm this destroy action.

- If you want to avoid this confirmation, you can auto approve by running:

```sh
    $ terraform destroy -var-file secrets.tfvars -auto-approve
```

## Reference documents and links

- https://docs.moodle.org/310/en/Installing_Moodle
- https://docs.moodle.org/310/en/Apache
- https://docs.moodle.org/310/en/PHP
- https://docs.moodle.org/310/en/PostgreSQL
- https://docs.moodle.org/310/en/Administration_via_command_line
- https://docs.moodle.org/310/en/Configuration_file
- https://docs.moodle.org/310/en/Transitioning_to_HTTPS
- https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/SSL-on-amazon-linux-2.html
- https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html
- https://www.terraform.io/downloads.html
