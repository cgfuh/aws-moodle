#!/bin/bash

yum -y update

# Apache +ssl

yum install -y httpd.x86_64
yum install -y mod_ssl

# PHP7.4 and extensions

amazon-linux-extras enable php7.4
yum install -y php \
    php-pear php-xml php-mbstring php-pgsql php-pdo \
    php-opcache php-zip php-zts php-json \
    php-gd php-soap php-xmlrpc php-intl
yum install -y memcached

# PostgreSQL

amazon-linux-extras enable postgresql11
yum clean metadata
amazon-linux-extras install postgresql11
yum install -y postgresql-server

# HTTP Settings

echo "AcceptPathInfo On" > /etc/httpd/conf.d/moodle.conf

# Generate a self-signed dummy certificate
cd /etc/pki/tls/certs
./make-dummy-cert moodle-localhost.crt

cat <<'EOF' >> /etc/httpd/conf.d/moodle.conf
<VirtualHost *:443>
    SSLEngine On
    SSLCertificateFile /etc/pki/tls/certs/moodle-localhost.crt
</VirtualHost>
EOF

/usr/bin/postgresql-setup --initdb

# Starting/Enabling services

systemctl start httpd.service
systemctl enable httpd.service

systemctl start postgresql
systemctl enable postgresql

# Moodle
cd /home/ec2-user
wget https://download.moodle.org/download.php/direct/stable310/moodle-${MDL_VERSION}.tgz

cd /var/www/html
tar xvfz /home/ec2-user/moodle-${MDL_VERSION}.tgz

chown -R root /var/www/html/moodle
chmod -R 0755 /var/www/html/moodle

sudo -u postgres psql -c "CREATE USER ${MDL_DBUSER} WITH PASSWORD '${MDL_DBPASS}';"
sudo -u postgres psql -c "CREATE DATABASE ${MDL_DBNAME} WITH OWNER ${MDL_DBUSER};"

sed -i '/^# IPv4 local connections.*/a host    moodle          moodleuser      127.0.0.1/32            password' /var/lib/pgsql/data/pg_hba.conf

systemctl restart postgresql

mkdir /var/www/moodledata
chmod 0777 /var/www/moodledata

chown -R apache:apache /var/www/html/moodle
cd /var/www/html/moodle/admin/cli

PUBLIC_IP=$(curl http://169.254.169.254:80/latest/meta-data/public-ipv4)

sudo -u apache /usr/bin/php install.php \
    --wwwroot=https://$PUBLIC_IP/moodle \
    --dataroot=/var/www/moodledata \
    --dbtype=pgsql \
    --dbhost=${MDL_DBHOST} \
    --dbname=${MDL_DBNAME} \
    --dbuser=${MDL_DBUSER} \
    --dbpass=${MDL_DBPASS} \
    --fullname=Moodle \
    --shortname=Moodle \
    --summary=Moodle \
    --adminuser=${MDL_ADMIN_ACCOUNT} \
    --adminpass=${MDL_ADMIN_ACCOUNT_PASSW} \
    --adminemail=${MDL_ADMIN_ACCOUNT_EMAIL} \
    --lang=en \
    --non-interactive \
    --agree-license
