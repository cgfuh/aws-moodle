terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.34.0"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.region
}

resource "aws_instance" "moodledemo" {
  ami           = "ami-0c27c96aaa148ba6d"
  instance_type = "t2.micro"
  key_name      = var.key_pair
  user_data     = data.template_file.user_data.rendered
  subnet_id     = aws_subnet.moodle-public.id
  #vpc_security_group_ids = ["${var.moodle_ssh_sg_id}"]
  vpc_security_group_ids = [
    aws_security_group.moodle-ssh-sg.id,
    aws_security_group.moodle-https-sg.id
  ]

  tags = {
    Name = var.instance_name
  }
}

data "template_file" "user_data" {
  template = file("../scripts//ec2_user_data.sh")
  #template = file("${path.module}/ec2_user_data.sh")

  vars = {
    MDL_VERSION             = var.mdl_version
    MDL_DBHOST              = var.mdl_dbhost
    MDL_DBNAME              = var.mdl_dbname
    MDL_DBUSER              = var.mdl_dbuser
    MDL_DBPASS              = var.mdl_dbpass
    MDL_ADMIN_ACCOUNT       = var.mdl_admin_acc
    MDL_ADMIN_ACCOUNT_PASSW = var.mdl_admin_acc_passw
    MDL_ADMIN_ACCOUNT_EMAIL = var.mdl_admin_acc_email
  }
}
