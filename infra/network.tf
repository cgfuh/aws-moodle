# VPC and networking stuff

resource "aws_vpc" "moodlevpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "moodle-dev"
  }
}

resource "aws_subnet" "moodle-public" {
  vpc_id                  = aws_vpc.moodlevpc.id
  cidr_block              = "10.0.40.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "sa-east-1a"

  tags = {
    Name = "moodle-dev-public"
  }
}

resource "aws_subnet" "moodle-private" {
  vpc_id                  = aws_vpc.moodlevpc.id
  cidr_block              = "10.0.41.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "sa-east-1c"

  tags = {
    Name = "moodle-dev-private"
  }
}

resource "aws_internet_gateway" "moodle-gw" {
  vpc_id = aws_vpc.moodlevpc.id

  tags = {
    Name = "moodle-dev-gw"
  }
}

resource "aws_route_table" "moodle-route-public" {
  vpc_id = aws_vpc.moodlevpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.moodle-gw.id
  }

  tags = {
    Name = "moodle-route-public-1"
  }
}

resource "aws_route_table_association" "moodle-route-public-1-a" {
  subnet_id      = aws_subnet.moodle-public.id
  route_table_id = aws_route_table.moodle-route-public.id
}


# Security


resource "aws_security_group" "moodle-ssh-sg" {
  description = "Controls SSH access to the Moodle instance"

  vpc_id = aws_vpc.moodlevpc.id
  name   = "moodle-ssh-sg"

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
}

resource "aws_security_group" "moodle-https-sg" {
  description = "Controls HTTPS access to the Moodle instance"

  vpc_id = aws_vpc.moodlevpc.id
  name   = "moodle-https-sg"

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
}

output "moodle_ssh_sg_id" {
  value = aws_security_group.moodle-ssh-sg.id
}

output "moodle_https_sg_id" {
  value = aws_security_group.moodle-https-sg.id
}

output "moodle_url" {
  description = "URL of Moodle instance"
  value       = "https://${aws_instance.moodledemo.public_dns}/moodle"
}
