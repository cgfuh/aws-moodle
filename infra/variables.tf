variable "region" {
  default = "sa-east-1"
}

variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "Moodle ExampleInstance"
}

variable "key_pair" {
  description = "AWS EC2 Key Pair Pem"
}

variable "mdl_version" {
  type        = string
  description = "Moodle version to be deployed"
  default     = "3.10.3"
}

variable "mdl_dbhost" {
  type        = string
  description = "Moodle DB host"
}

variable "mdl_dbname" {
  type        = string
  description = "Moodle DB name"
}

variable "mdl_dbuser" {
  type        = string
  description = "Moodle DB user"
}

variable "mdl_dbpass" {
  type        = string
  description = "Moodle DB user password"
}

variable "mdl_admin_acc" {
  type        = string
  description = "Moodle Admin account"
}

variable "mdl_admin_acc_passw" {
  type        = string
  description = "Moodle Admin account password"
}

variable "mdl_admin_acc_email" {
  type        = string
  description = "Moodle Admin account email"
}

variable "docker_img_name" {
  type        = string
  description = "Name of the docker image being deployed"
  default     = "caio/moodle"
}

variable "docker_img_tag" {
  type        = string
  description = "The docker image TAG being deployed"
  default     = "latest"
}

